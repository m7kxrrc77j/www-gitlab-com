---
layout: handbook-page-toc
title: "Verify Stage"
description: "The verify development group handbook page."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

We enable global software organizations and teams to make great decisions with [smart feedback loops](https://about.gitlab.com/direction/ops/#smart-feedback-loop) by delivering [speedy, reliable pipelines](/direction/ops/#speedy-reliable-pipelines) in a [comprehensive CI platform](/direction/ops/#verify) that embodies [Operations for All](/direction/ops/#operations-for-all).

## Mission

As engineers in Verify we know our customers because we _are_ our customers, and we are constantly striving to make our platform better for everyone. We do this through [iteration](/handbook/values/#iteration), [dogfooding](/handbook/values/#dogfooding), and being involved in our open source community. We innovate, we collaborate, and we challenge assumptions to arrive at great results.

We take ownership of the things we build, with a focus on stability and availability. We do this by having a deep technical understanding of the operation and performance characteristics of our platform, and a proactive perspective to future growth.

## Who we are

The Verify stage is made up of 4 groups:

1. [Verify:Pipeline Execution](/handbook/engineering/development/ops/verify/pipeline-execution/)
1. [Verify:Pipeline Authoring](/handbook/engineering/development/ops/verify/pipeline-authoring/)
1. [Verify:Runner](/handbook/engineering/development/ops/verify/runner/)
1. [Verify:Pipeline Insights](/handbook/engineering/development/ops/verify/pipeline-insights/)

## UX Strategy

For of our UX Vision and Strategy, take a look at the [Verify UX Strategy](/handbook/engineering/ux/stage-group-ux-strategy/verify/) page.

### Jobs to be done (JTBD)

A [Job to be Done (JTBD)](/handbook/engineering/ux/jobs-to-be-done/) is a framework, or lens, for viewing products and solutions in terms of the jobs customers are trying to achieve.

* [Verify:Pipeline Execution JTBD](/handbook/engineering/development/ops/verify/pipeline-execution/jtbd/)
* [Verify:Pipeline Authoring JTBD](/handbook/engineering/development/ops/verify/pipeline-authoring/jtdb/)
* [Verify:Runner JTBD](/handbook/engineering/development/ops/verify/runner/jtbd/)
* [Verify:Pipeline Insights JTBD](/handbook/engineering/development/ops/verify/pipeline-insights/JTBD/)

### Pipeline Authoring and Pipeline Execution Collaboration

Pipeline Authoring and Pipeline Execution are closely related but they also represent different stages in the cycle of a user's interaction with a pipeline. At a very high-level, this image illustrates the main focus of each group and how they can both support a better pipeline experience.

![Verify Groups](/images/handbook/engineering/verify/verify_groups_banner.jpg)

### Verify:Pipeline Execution

<%= direct_team(manager_role: 'Backend Engineering Manager, Verify:Pipeline Authoring & Acting Backend Engineering, Verify:Pipeline Execution', role_regexp: /Pipeline Execution/) %>

<%= direct_team(manager_role: 'Frontend Engineering Manager, Verify', role_regexp: /Pipeline Execution/) %>

### Verify:Pipeline Authoring

<%= direct_team(manager_role: 'Backend Engineering Manager, Verify:Pipeline Authoring & Acting Backend Engineering, Verify:Pipeline Execution', role_regexp: /Pipeline Authoring/) %>

<%= direct_team(manager_role: 'Frontend Engineering Manager, Verify', role_regexp: /Pipeline Authoring/) %>

### Verify:Runner

<%= direct_team(manager_role: 'Backend Engineering Manager, Verify:Runner') %>

### Verify:Pipeline Insights

<%= direct_team(manager_role: 'Fullstack Engineering Manager, Verify:Pipeline Insights') %>

## How we work

Each group in Verify is encouraged to define their own process based on what works best for them. Individual group processes are described on the pages below:

* [Verify:Pipeline Execution](/handbook/engineering/development/ops/verify/pipeline-execution/)
* [Verify:Pipeline Authoring](/handbook/engineering/development/ops/verify/pipeline-authoring/)
* [Verify:Runner](/handbook/engineering/development/ops/verify/runner/)
* [Verify:Pipeline Insights](/handbook/engineering/development/ops/verify/pipeline-insights/)

### Shared issues

In the Verify team we lean in to the GitLab mission, "[everyone can contribute](https://about.gitlab.com/company/mission/#mission)"!
To help balance this workload out across the groups, we use the [`Verify candidate`](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Verify+candidate) label.
Every issue with this label is a good candidate to be worked on by any group in the Verify stage.
This applies to both frontend and backend issues.
Prioritization is still determined by the Product Manager, such as ensuring any deliverables in the engineer's own group still take priority, but engineers are encouraged to pick work up from [this board](https://gitlab.com/gitlab-org/gitlab/-/boards/1339417?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Verify%20candidate).
This helps us break down silos, balance the workload, and prevent disruptive re-allocations.

To help with prioritizing within the list of available `Verify candidate` issues, it's recommended to reference the issue types in the [Product Priorities](https://about.gitlab.com/handbook/product/product-processes/#prioritization) list, noting any severities applied on the issues as well.

### Issue Health Status Definitions & Async Issue Updates 

Across Verify we value [Transparency](/handbook/values/#transparency), we live our values of [Inclusion](/handbook/values/#bias-towards-asynchronous-communication), and we expect [Efficiency](https://about.gitlab.com/handbook/values/#write-things-down) by dogfooding using Issue Health Statuses and providing regular updates on active issues. Each team in the Verify Stage will define the cadence of updates and specific defintion of the statuses, but generally the expectation is a weekly update on in progress issues with the following Health Statuses: 

- On Track
- Needs Attention
- At Risk

These updates are an opportunity for the engineer to add detail to the status and are not expected to provide a justification for why something is behind or will miss a milestone. We encourage [blameless problem solving](/handbook/values/#blameless-problem-solving) and [kindness](/handbook/values/#kindness) at all times. 

### Merge Requests in Verify 

In the Verify stage, we value [MR Rate](https://about.gitlab.com/handbook/engineering/metrics/#merge-request-rate) as a shared performance indicator for team collaboration, iteration, customer results. The entire team is responsible for iterative scope in issues. This starts with product management creating a clear problem statement connected to user insights. UX then adds interaction specifications and acceptance criteria to then be considered and weighed by the engineering team. Teams are encouraged to iterate on scope so as to delivery the [smallest thing possible](https://about.gitlab.com/handbook/values/#iteration).

By considering MR Rate as a measure of throughput, product management is focused on creating decomposed pieces of scope to improve the user experience. This encourages the UX and engineering teammates to provide simpler ways to solve the same problem, ultimately improving the throughput of the entire team.

### Decoupled project management

Many of the groups in CI/CD have separate Frontend and Backend Engineering Managers. Given each group's ability to define their own process, groups may also consider decoupling project management functions from people management functions.

For example, a group's managers could determine that one of them will be the DRI for a given project, or the entire group. That individual would then be responsible for planning for the whole team, instead of splitting the planning responsiblity between two managers which requires extra coordination.

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%=
role_regexp = /[,&] Verify/
direct_manager_role = 'Senior Manager, Engineering, Verify'
other_manager_roles = [
  'Frontend Engineering Manager, Verify',
  'Backend Engineering Manager, Verify:Pipeline Authoring & Acting Backend Engineering, Verify:Pipeline Execution',
  'Backend Engineering Manager, Verify:Runner',
  'Fullstack Engineering Manager, Verify:Pipeline Insights'
]
stable_counterparts(role_regexp: role_regexp, direct_manager_role: direct_manager_role, other_manager_roles: other_manager_roles)
%>

### Async Work Week

In FY23-Q1, we're piloting [async work weeks in Verify and have proposed 2021-02-13 to 2022-02-19](https://gitlab.com/gitlab-org/verify-stage/-/issues/178). We're considering doing this once per quarter in the Verify stage. 
Some of the benefits include reducing time spent in sync meetings, allowing for more focus, which aligns with our async-first communication and our Diversity and Inclusion value to bias towards more async communication.
However, this doesn't preclude us from having any meetings, and it's up to meeting facilitators to decide accordingly. Exceptions might include: high priority issues and initiatives, social calls, coffee catch-ups. 

| Quarter | Async Week               |
| ------- | ------------------------ |
| FY23-Q1 | 2021-02-13 to 2022-02-19 |


## Verify Engineering Update Newsletter

Every two weeks the Verify Engineering Update Newsletter is set out to an opt-in subscriber list. The purpose of the email is to share recent highlights from the Verify stage so folks will have a better idea of what is happening on other teams, and provide new opportunities for learning and collaboration.

Everyone is welcome to sign up or view previous issue on the [newsletter page](https://www.getrevue.co/profile/verify-engineering-update).

Each issue of the newsletter is planned using individual issues linked in the [newsletter epic](https://gitlab.com/groups/gitlab-com/-/epics/1148). Content is generally contributed by managers, but everyone is encouraged to contribute topics for the newsletter.

## #s_verify-updates

The Verify stage uses the [`#s_verify-updates`](https://gitlab.slack.com/archives/C022PTK9V6K) channel in Slack to communicate various updates relevant to the group. Everyone in the stage is encouraged to contribute any sort of team, product, or process update. This channel is also a good place to celebrate sucessess and thank fellow team members.  

## Verify Technical Discussions

Verify Technical Discussions is a Zoom meeting hosted bi-weekly by Verify Team. Everyone is invited, however paricipation of Verify stage members is especially encouraged.

During the meeting we discus a variety of technical aspects related to the Verify stage roadmap.

Everyone can add their points to the [agenda document](https://docs.google.com/document/d/1SEydi30hYjqZ5ellZgwcijUesRnWjQVd___h3J9SSKY).

Below you can find a table with links to recordings of Verify Technical Discussions and Technical Deep Dives.

| Date       | Title                                                          | Recording                                 |
|------------|----------------------------------------------------------------|-------------------------------------------|
| 2021-01-21 | Technical Discussions - Pipeline Editor and database storage   | [Recording](https://youtu.be/KsOR3lIz_4w) |
| 2021-01-07 | Technical Discussions - Next iteration of CI/CD Pipeline DAG   | [Recording](https://youtu.be/CvYEqSwd-UE) |
| 2020-12-10 | Technical Deep Dive - Observability at GitLab with demos       | [Recording](https://youtu.be/DVNyH3zQWBo) |
| 2020-11-19 | Technical Deep Dive - Cloud Native Build Logs feature overview | [Recording](https://youtu.be/Fq1ecmb_zk0) |
| 2020-05-08 | Technical Deep Dive - Using Prometheus with GitLab Compose Kit | [Recording](https://youtu.be/y9NW7A_XJrU) |

## Weekly Triage Reports

The Product Manager, Engineering Manager(s), and Designer for each group are responsible for triaging and scheduling [feature proposals and bugs as part of the Weekly Triage Report](https://about.gitlab.com/handbook/engineering/quality/triage-operations/#group-level-bugs-features-and-ux-debt). Product Managers schedule issues by assigning them to a Milestone or the Backlog.  For bug triage, Engineering Managers and Product Managers work together to ensure that the correct `severity` and `priority` labels have been applied.  Since [Product Managers are the DRIs for prioritization](https://about.gitlab.com/handbook/engineering/quality/issue-triage/#priority), they will validate and/or apply the initial `priority` label to bugs.  Engineering Managers are responsible for adding or updating the `severity` labels for bugs listed in the triage report, and to help Product Management with understanding the criticality and technical feasibility of the bug, as needed.

While SLOs for resolving bugs are tied to severities, it is up to Product Management to set priorities for the group with an appropriate target resolution time. For example, criteria such as the volume of `severity::2` level bugs may make it appropriate for the Product Manager to adjust the priority accordingly to reflect the expected time to resolution.

### Availability, security, performance, and bug triage process

In the Verify Stage, we aim to solve new availability, security, performance issues within the SLO of the assigned severity. These types of issues are the top priority followed by bugs and technical debt according to our [severity SLO chart](https://about.gitlab.com/handbook/engineering/quality/issue-triage/#severity-slos).

Availability and performance issues (commonly referred to as infradev) are also triaged in the [Infra/Dev Triage Board](https://gitlab.com/groups/gitlab-org/-/boards/1193197?label_name[]=gitlab.com&label_name[]=infradev).

## Supporting Community Contributions

We believe in supporting our open source community. We aim to support two main measure of success:

1. [Merged MRs from community contributions](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution&label_name[]=devops%3A%3Averify)
1. [MRARR](/handbook/engineering/quality/performance-indicators/#mrarr)

Each team in the Verify Stage follows roughly the same process to ensure the community is effectively supported and free to add features or fixes to the product. How we manage the Community Contribution MRs is spread across three main areas: processing the contributions, reviewing the contributions, and merging the contributions.

### Process

Code contributions to Verify typically occur in three flavors:

1. Free users, open source contributions from already scoped issues
1. Paid users, open source contributions not from scoped issues
1. Paid users, proprietary contributions not from scoped issues

Contributions from both free and paid users are equally important and will follow our [GitLab Contribution Guidelines](https://about.gitlab.com/community/contribute/). We strive to make this process as frictionless as possible between our users and the Engineering teams in Verify, especially during the reviewing of contributions.

### Reviewing contributions

Once a contribution has been created, the Engineering Manager assigns an engineer to manage and review with the Community Contributor. Reviewing contributions will follow the definition of done, style guidelines, and other practices of development. As the DRI of the review, the assigned Verify engineer will work with Community Contributor to resolve any outstanding items. The MR is then passed to a Maintainer with relevant domain expertise for final review prior to being merged.

#### Contributions from Partners 

Our partners are an important part of our ecosystem at GitLab. These contributions should be reviewed with the same [GitLab Contribution Guidelines](/community/contribute/) as community MR contributions, and aligns with the [Verify contribution guidelines](https://docs.gitlab.com/ee/development/contributing/verify/) for working in the Verify areas of the codebase.

### Merging the Contribution

The Maintainer of the codebase will be the DRI of merging the contribution into the Verify product.

### SLOs for Community Contributions 

For issues that are **refined**, they are considered priority for review and merging. Refined issues are defined as issues in `workflow::ready for development`, with `direction` labels, and either have technical proposals or are weighted. Issues that are not labeled with `workflow::ready for development` and `direction` labels are considered **non-refined** and are lower priority for review, and therefore have longer merge SLOs. SLOs for these two types of issues are defined in the table below:

| Types of issues & users  | Time Frame for Review SLO | Time frame for Merge SLO |
| --- | --- | --- |
| All users contributing to refined issues or bugs of severity S2 or S1  | 30 days | The next release (60 days) |
| Paying users from non-refined issues or bugs of severity S3 | 60 days | Within the next 3 releases (approx one quarter or 90 days) |
| Non-paying users from non-refined issues or bugs of severity S4 | 90 days | Anything outside the next 3 releases (more than one quarter or 120 days). |

In order to prevent the inflow of Community Contributions overwhelming engineers on the team and impacting their ability to work on planned issues, there is a WIP limit of 5 assigned Community Contribution MRs per reviewing engineer. This helps limit the amount of context switching a single engineer is doing and prevents them from being overwhelmed with reviews.

## Common Links

### Slack Channels

* Verify [#s_verify](https://gitlab.slack.com/archives/C0SFP840G)
  * Verify:Pipeline Execution [#g_pipeline-execution](https://gitlab.slack.com/archives/CPCJ8CCCX)
  * Verify:Pipeline Authoring [#g_pipeline-authoring](https://gitlab.slack.com/archives/C019R5JD44E)
  * Verify:Runner [#g_runner](https://gitlab.slack.com/archives/CBQ76ND6W)
  * Verify:Pipeline Insights [#g_pipeline-insights](https://gitlab.slack.com/archives/CPANF553J)
* Verify Engineering Management [#s_verify-em](https://gitlab.slack.com/archives/C014UTH4W02)
* Verify Frontend Engineering [#s_verify_fe](https://gitlab.slack.com/archives/CUYH1MP0Q)
* CI/CD UX [#ux_ci-cd](http://gitlab.slack.com/archives/CL9STLJ06)
