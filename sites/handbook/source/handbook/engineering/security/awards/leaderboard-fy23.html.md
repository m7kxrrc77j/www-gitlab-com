---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY23

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@leipert](https://gitlab.com/leipert) | 1 | 1600 |
| [@vitallium](https://gitlab.com/vitallium) | 2 | 900 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 3 | 900 |
| [@tkuah](https://gitlab.com/tkuah) | 4 | 850 |
| [@kassio](https://gitlab.com/kassio) | 5 | 600 |
| [@brodock](https://gitlab.com/brodock) | 6 | 600 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 7 | 600 |
| [@cam_swords](https://gitlab.com/cam_swords) | 8 | 600 |
| [@.luke](https://gitlab.com/.luke) | 9 | 560 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 10 | 540 |
| [@ratchade](https://gitlab.com/ratchade) | 11 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 12 | 500 |
| [@jlear](https://gitlab.com/jlear) | 13 | 500 |
| [@acunskis](https://gitlab.com/acunskis) | 14 | 500 |
| [@peterhegman](https://gitlab.com/peterhegman) | 15 | 500 |
| [@sgoldstein](https://gitlab.com/sgoldstein) | 16 | 500 |
| [@atiwari71](https://gitlab.com/atiwari71) | 17 | 500 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 18 | 460 |
| [@garyh](https://gitlab.com/garyh) | 19 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 20 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 21 | 400 |
| [@kerrizor](https://gitlab.com/kerrizor) | 22 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 23 | 300 |
| [@toupeira](https://gitlab.com/toupeira) | 24 | 240 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 25 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 26 | 150 |
| [@jerasmus](https://gitlab.com/jerasmus) | 27 | 140 |
| [@pshutsin](https://gitlab.com/pshutsin) | 28 | 140 |
| [@10io](https://gitlab.com/10io) | 29 | 100 |
| [@mattkasa](https://gitlab.com/mattkasa) | 30 | 100 |
| [@alexpooley](https://gitlab.com/alexpooley) | 31 | 100 |
| [@ifarkas](https://gitlab.com/ifarkas) | 32 | 100 |
| [@dmakovey](https://gitlab.com/dmakovey) | 33 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 34 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 35 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 36 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 37 | 60 |
| [@minac](https://gitlab.com/minac) | 38 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 39 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 40 | 60 |
| [@pedropombeiro](https://gitlab.com/pedropombeiro) | 41 | 60 |
| [@lauraX](https://gitlab.com/lauraX) | 42 | 60 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 43 | 60 |
| [@bala.kumar](https://gitlab.com/bala.kumar) | 44 | 60 |
| [@dblessing](https://gitlab.com/dblessing) | 45 | 60 |
| [@cngo](https://gitlab.com/cngo) | 46 | 60 |
| [@ebaque](https://gitlab.com/ebaque) | 47 | 50 |
| [@mbobin](https://gitlab.com/mbobin) | 48 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 49 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 50 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 51 | 40 |
| [@balasankarc](https://gitlab.com/balasankarc) | 52 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 53 | 30 |
| [@subashis](https://gitlab.com/subashis) | 54 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 55 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 56 | 30 |
| [@furkanayhan](https://gitlab.com/furkanayhan) | 57 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 58 | 30 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 59 | 30 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 60 | 30 |
| [@ealcantara](https://gitlab.com/ealcantara) | 61 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 62 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 63 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 64 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 1 | 400 |
| [@f_santos](https://gitlab.com/f_santos) | 2 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 3 | 300 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 4 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 5 | 30 |
| [@john.mcdonnell](https://gitlab.com/john.mcdonnell) | 6 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vburton](https://gitlab.com/vburton) | 1 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rpadovani](https://gitlab.com/rpadovani) | 1 | 1200 |
| [@feistel](https://gitlab.com/feistel) | 2 | 400 |
| [@spirosoik](https://gitlab.com/spirosoik) | 3 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 4 | 200 |
| [@zined](https://gitlab.com/zined) | 5 | 200 |

## FY23-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@leipert](https://gitlab.com/leipert) | 1 | 1600 |
| [@vitallium](https://gitlab.com/vitallium) | 2 | 900 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 3 | 900 |
| [@tkuah](https://gitlab.com/tkuah) | 4 | 850 |
| [@kassio](https://gitlab.com/kassio) | 5 | 600 |
| [@brodock](https://gitlab.com/brodock) | 6 | 600 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 7 | 600 |
| [@cam_swords](https://gitlab.com/cam_swords) | 8 | 600 |
| [@.luke](https://gitlab.com/.luke) | 9 | 560 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 10 | 540 |
| [@ratchade](https://gitlab.com/ratchade) | 11 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 12 | 500 |
| [@jlear](https://gitlab.com/jlear) | 13 | 500 |
| [@acunskis](https://gitlab.com/acunskis) | 14 | 500 |
| [@peterhegman](https://gitlab.com/peterhegman) | 15 | 500 |
| [@sgoldstein](https://gitlab.com/sgoldstein) | 16 | 500 |
| [@atiwari71](https://gitlab.com/atiwari71) | 17 | 500 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 18 | 460 |
| [@garyh](https://gitlab.com/garyh) | 19 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 20 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 21 | 400 |
| [@kerrizor](https://gitlab.com/kerrizor) | 22 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 23 | 300 |
| [@toupeira](https://gitlab.com/toupeira) | 24 | 240 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 25 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 26 | 150 |
| [@jerasmus](https://gitlab.com/jerasmus) | 27 | 140 |
| [@pshutsin](https://gitlab.com/pshutsin) | 28 | 140 |
| [@10io](https://gitlab.com/10io) | 29 | 100 |
| [@mattkasa](https://gitlab.com/mattkasa) | 30 | 100 |
| [@alexpooley](https://gitlab.com/alexpooley) | 31 | 100 |
| [@ifarkas](https://gitlab.com/ifarkas) | 32 | 100 |
| [@dmakovey](https://gitlab.com/dmakovey) | 33 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 34 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 35 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 36 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 37 | 60 |
| [@minac](https://gitlab.com/minac) | 38 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 39 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 40 | 60 |
| [@pedropombeiro](https://gitlab.com/pedropombeiro) | 41 | 60 |
| [@lauraX](https://gitlab.com/lauraX) | 42 | 60 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 43 | 60 |
| [@bala.kumar](https://gitlab.com/bala.kumar) | 44 | 60 |
| [@dblessing](https://gitlab.com/dblessing) | 45 | 60 |
| [@cngo](https://gitlab.com/cngo) | 46 | 60 |
| [@ebaque](https://gitlab.com/ebaque) | 47 | 50 |
| [@mbobin](https://gitlab.com/mbobin) | 48 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 49 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 50 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 51 | 40 |
| [@balasankarc](https://gitlab.com/balasankarc) | 52 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 53 | 30 |
| [@subashis](https://gitlab.com/subashis) | 54 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 55 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 56 | 30 |
| [@furkanayhan](https://gitlab.com/furkanayhan) | 57 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 58 | 30 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 59 | 30 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 60 | 30 |
| [@ealcantara](https://gitlab.com/ealcantara) | 61 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 62 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 63 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 64 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 1 | 400 |
| [@f_santos](https://gitlab.com/f_santos) | 2 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 3 | 300 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 4 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 5 | 30 |
| [@john.mcdonnell](https://gitlab.com/john.mcdonnell) | 6 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vburton](https://gitlab.com/vburton) | 1 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rpadovani](https://gitlab.com/rpadovani) | 1 | 1200 |
| [@feistel](https://gitlab.com/feistel) | 2 | 400 |
| [@spirosoik](https://gitlab.com/spirosoik) | 3 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 4 | 200 |
| [@zined](https://gitlab.com/zined) | 5 | 200 |


