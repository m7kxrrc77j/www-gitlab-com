---
layout: handbook-page-toc
title: "Security incident communications plan"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

GitLab takes the security of our clients’ information extremely seriously, regardless of whether it’s on GitLab.com or in a self-managed instance. In keeping with GitLab’s [value of transparency](/handbook/values/#transparency) we believe in communicating about security incidents clearly and promptly.

This communication response plan maps out the who, what, when, and how of GitLab in notifying and engaging with internal stakeholders and external customers on security incidents. **This plan of action covers the strategy and approach for security events which have a ‘high’ or greater impact as outlined in [GitLab’s risk scoring matrix](/handbook/engineering/security/security-assurance/security-risk/storm-program/storm-methodology.html#risk-factors-and-risk-scoring).**

## What is an incident?
The GitLab Security team identifies security incidents as any violation, or threat of violation, of GitLab security, acceptable use or other relevant policies.  You can learn more about how we identify incidents in the [GitLab security incident response guide](/handbook/engineering/security/security-operations/sirt/sec-incident-response.html#incident-identification).

### 💁 Corporate incident response
For Support or Infrastructure managed incidents where external communication guidance is needed, please see the [corporate communications incident response plan](https://about.gitlab.com/handbook/marketing/corporate-marketing/incident-communications-plan/#defining-the-scopeseverity-of-a-potential-incident) and engage that team via #corp-comms in slack.

### 👷 Infrastructure incident response
For Infrastructure incidents, please follow the [infrastructure incident management and communication process](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#communication).

## Defining the scope/severity of an incident
The `Security Engineer On-Call` will determine the scope, severity and [potential impact](/handbook/engineering/security/security-assurance/security-risk/storm-program/storm-methodology.html#risk-factors-and-risk-scoring) of the security incident. Once the potential impact has been determined, implementation of the appropriate internal and external communications strategy should begin.

# Roles and responsibilities in a Security incident

## Security team roles and responsibilities
* **Security Engineer on Call (SEOC):** This is the on-call Security Operations Engineer. The individual is the first to act, validate, and begin the process of determining severity and scope.

* **Security Incident Manager on Call (SIMOC):** This is a Security Engineering Manager who is engaged when incident resolution requires coordination across multiple parties. The SIMOC is the tactical leader of the incident response team, typically not engaged to perform technical work. The SIMOC assembles the incident team by engaging individuals with the skills, access, and information required to resolve the incident. The focus of the SIMOC is to keep the incident moving towards resolution, keeping stakeholders informed and performing CMOC duties.

* **Communications Manager on Call (CMOC):** This is the Security Incident Manager On-Call (SIMOC) or Security Assurance Engineer who will coordinate external communications efforts according to this security incident response plan and liaise across the extended GitLab teams to ensure all parties are activated, updated and aligned.

* **Security External Communications:** this function partners with and advises incident response teams in reviewing and improving messaging for external audiences (including customers, media, broader industry). This role laises with marketing teams for any necessary reviews or messaging deployment. This function should be engaged **once first draft content has been developed** using the [Security incident external response issue template](https://gitlab.com/gitlab-com/gl-security/security-communications/communications/-/blob/master/.gitlab/issue_templates/security-external-incident-or-event-response-template.md).


### <i class="fas fa-arrow-right" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i> More about the CMOC responsibilities
{: #arrow-purple}

As security practitioners and incident response engineers, our security assurance and security operations teams and engineers are best positioned to develop initial messaging and serve in the `CMOC`/`Communications manager on call` role.

**Each team-appointed `CMOC` is the DRI for**: 
* Opening the [Security incident external response issue template](https://gitlab.com/gitlab-com/gl-security/security-communications/communications/-/blob/master/.gitlab/issue_templates/security-external-incident-or-event-response-template.md) and tagging in potential stakeholders and contributors
* Drafting the initial message(s) 
* Tracking development and deployment of the various content and 
* Identifying key stakeholders for contribution, review and approval (support, security/SIRT leadership, legal, etc)
* Routing and securing approvals from various parties (support, security/SIRT leadership, legal, etc)
* Keeping stakeholders updated and informed on the progress of communications development in the related incident response slack channel and issue(s)

### <i class="fas fa-arrow-right" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i> More about the Security External Communications function responsibilities
{: #arrow-purple}

**The `Security External Communications function` is the DRI for:**
* Editing and improving first drafts **provided by** `CMOC`
* Advising on appropriate channels and forms of communication needed 
* Acting as an approval point on final messaging to ensure it’s ready for external use
* Liaising with PR and corporate communications for additional reviews and/or messaging needs (public/media statements)
* Deploying the messaging via collaboration with our PR (media statement), Content Marketing (blog post) and Marketing Operations teams (email response)
* Posting final communications materials to slack channels (`#community-relations`, `#social_media_action`, `#sales`, `#security-department` and `#customer-success`) for awareness and use.
     * `Support manager on call` will manage support team awareness
     * **The following groups are auto-tagged on each communications tracking issue: PR, Support, Field Security, Customer Success, Legal, Community Relations and Security department leadership** 

## Extended team roles, responsibilities and points of contact

- **Marketing Operations:** Responsible for sending incident-related email to impacted parties in a security incident.  This group has established a [Marketing emergency response process](https://about.gitlab.com/handbook/marketing/emergency-response/#marketing-emergency-response) and is engaged by [creating an incident communication request](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=incident_communications) using the `incident_communications` template and posting the issue in the #mktgops channel in Slack. 
     - Marketing Ops can send emails through MailGun or Marketo. This group will determine based on the information provided what the best platform is for distribution. If a custom distribution list needs to be be created, the data team may need to be involved. 
     - For urgent issues, if you do not receive a prompt response in slack, page the on-call marketing ops via entering `/pd trigger` command in any slack channel and select `Marketing Ops Ext. Comms - Emergency`.

- **Support Team:** Using background information and prepared responses provided by the Security Engineer On Call and Communications Manager On Call, our Support Team will triage and respond to customer communications stemming from the security incident. [Contact the on-call manager via `#spt_managers` in Slack](/handbook/support/internal-support/#on-slack). If it's urgent [page the `Support Manager On-call`](/handbook/support/on-call/#paging-the-on-call-manager) using `/pd-support-manager` command in slack. To ensure this group has early awareness on security incidents and events they are autotagged as an FYI in the security-external-incident-or-event-response template.

- **Community Relations:** May need to respond to customers and the general public via social channels, as such should be engaged before public-facing materials are released. Any prepared responses or FAQs should be provided to assist with their interactions. Contact this group in `#community-relations` or any Slack channel by pinging `@community-team`. To ensure this group has early awareness on security incidents and events they are autotagged as an FYI in the `security-external-incident-or-event-response` template. 

- **Designated Approvers:** This is the group of individuals who act as reviewers and approvers across each piece of communications developed for a security incident. It includes representatives from Security, Support, Customer Success, Legal, Corporate Communications and Investor Relations.

# Communicating internally

Security incidents can be high-pressure, high-stress situations.  Everyone is anxious to understand the details around the investigation, scope, mitigation and more. Ensuring that stakeholders across security, infrastructure, engineering and operations teams are informed and engaged is one of the chief responsibilities of the Security Incident Manager On Call. The `Security Incident Manager On Call` should focus on providing high-level status updates without delving too deeply into the technical details of the incident, including:
* Current Risk
* Users Impacted (some, many, all?)
* Services Impacted (production, enterprise apps, other)
* Timeline of events
* Mitigation steps that have been taken
* Current status of the incident
* Next steps

## Communicating with GitLab team members
Any time there is a service disruption for team members, the CMOC should post details regarding the disruption and related issue(s) in #whats-happening-at-gitlab, and cross-post in any related channels. It is important to identify if this is a production incident affecting gitlab.com or a service used by the organization.

### Internal Security Notification Dashboard
In cases of high priority security notifications appropriate for the entire organization, the [Internal Security Notification Dashboard](https://gitlab.com/gitlab-com/gl-security/internal-security-notification-dashboard/-/wikis/*Dashboard:-GitLab-Internal-Security-Notifications*) should be used. When an update is made to this dashboard, notifications will be sent via slack and email to all GitLab team members.

### Incident response channel on Slack
In the cases of incidents that are on-going and require constant communication the `Security Engineer on Call` will set up an incident response Slack channel. All security incident team members and extended POCs should be invited. If the nature of the incident allows, the Slack channel will be public to GitLab and a link to this channel will also be shared in `#security-department` Slack channel to increase visibility.

### Engaging key internal stakeholders (when/how)

| Group & Contacts | When to Engage | DRI to Engage | At what Cadence | In what Channel |
| ------ | ------ | ------ | ------ | ------ |
| Director of Security Operations | For S1 incidents immediately upon determination of the S1 severity rating | `SIMOC/CMOC` | 30 minute intervals (unless otherwise requested) | In incident response Slack channel |
| VP of Security | For S1 incidents immediately upon determination of the S1 severity rating | `Director of Security Operations` | 30 minute intervals (unless otherwise requested) | Slack direct message |
| Broader e-group | Immediately in cases of a data breach or an RCE with evidence of exploitation | `VP of Security` | 30 minute intervals (unless otherwise requested) | `#e-group` Slack channel |
| Sr. Director of Corporate Marketing and Director of Corporate Communications | Immediately, if the incident has been publicly reported or if there is a regulatory requirement to make an announcement. In other cases, once the full impact and associated risk has been determined. | `SIMOC/CMOC` | Continuous | In incident response Slack channel |
| Legal | If GitLab EE customers are impacted, or if the security incident includes a data breach including but not limited to: <br> Exposure of PII / Personal Data <br> Private Projects <br> Financial Information | `VP of Security` | Continuous | Incident response Slack channel |
| `Designated key approvers`| As soon as we know we'll need to communicate with customers | `CMOC` | Continuous | Incident response Slack channel |


# Communicating externally  

External communications should happen as soon as possible after the scope and impact of the security incident is determined, using concise and clear language. The first external communications are directed to affected parties. Examples include: Affected customers and third parties (including [JiHu](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/)), providers of products, services or organizations involved in the supply chain for system components as related to the incident. Regulatory authorities are contacted based on incident scope, regulatory and legal requirements.

## Turnaround on customer messaging
Once it has been determined that external response is needed, the SIRT team should develop, gain approval on a final customer communication and distribute and/or publish within 24 hours.

## Communications channels and forms
The communications channels and forms that should be used in an incident or event can vary but should align with our need to be responsive to our customers and our transparency value, and be balanced with the potential risk and exposure to customers.

**Commonly used forms and channels of communication:**
- Our most common form of customer response is via direct email communications to affected customers.
- When a deeper dive response is needed, or to ensure broader coverage on a security incident or event, a blog post may be developed on an urgent basis.
- See [deeper dive explanations on forms and channels](/handbook/engineering/security/security-operations/sirt/security-incident-communication-plan.html#potential-channels-for-use-in-a-security-incident) for consideration
- Communication to [JiHu](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/) should happen via the `#engineering-security` channel within the JiHu Slack workspace. GitLab team members Dominic Couture, James Ritchey, Bistra Lutz, Laurence Bierner, Jerome Ng, Mek Stittri and Kenny Johnston have access to this channel.

## Helpful templates and runbooks
* 👉 Our [`Security external incident or event response template`](https://gitlab.com/gitlab-com/gl-security/security-communications/communications/-/blob/master/.gitlab/issue_templates/security-external-incident-or-event-response-template.md) (this is an internal template) has links to templates that can be used (make a copy)to start various communications.

* 📝 Security incident communications runbooks are located [here](https://gitlab.com/gitlab-com/gl-security/runbooks/-/tree/master/communications) (internal only).

## Preparing and enabling external facing teams
It is important to keep in mind, **any time we are communicating externally, we need to advise our support, customer, social and community relations teams that we’ll be making external communication about an issue that affects customers and/or the community**. 

For this reason, each incident response (direct email, media statement, blog post, etc) should have accompanying:
- Social media statement
- FAQs for our Support and customer-facing teams


## On-going, live incidents on GitLab.com
For on-going, live site incidents on GitLab.com, updates are provided by the `SIMOC/CMOC` through status.io to [https://status.gitlab.com/](https://status.gitlab.com/) and the [@GitLabStatus](https://twitter.com/GitLabStatus) twitter handle.

## External statements and other public, official communications
Depending on scope, impact or risk associated with the incident, our Corporate Communications and Marketing team may determine that additional outreach is necessary. Any official statements about the security incident would be made by GitLab’s Director of Corporate Communications, VP of Corporate Marketing, CMO or VP of Security.

# Process for security incidents external communications

1. Once we’ve determined that we need to make an external statement or communicate with customers in some way, [open a new issue using the Security incident external response issue template](https://gitlab.com/gitlab-com/gl-security/security-communications/communications/-/blob/master/.gitlab/issue_templates/security-external-incident-or-event-response-template.md)This issue will be used by the `SIMOC/CMOC` to track content development, reviews and approvals. 
2. The new security incident external response issue should be assigned to the `CMOC`
3. Once all the actions under the “CMOC actions to take upon opening this issue” section are completed and a first draft(s) is/are in place, tag `@heather` into the issue for first review and consult on communications forms/channels (more details below). 
     - For urgent issues/incidents, tag `@heather` in slack.

## Communications review, approval and deployment process

1. Once a draft is ready, the CMOC should tag `@heather` in the related security incident external response issue for first round review and edits. 
2. In parallel, our `Security External Communications` will engage the appropriate marketing teams for support (PR, Marketing Ops, Content teams) and begin creating the related issues for parallel marketing support and message deployment efforts.
3. When this first round of review and edits are complete, the `CMOC` should route the communication doc to the `designated key approvers` as outlined below for their collaboration and review. As security incidents are urgent, this team gets 30 minutes to review and edit the doc. Reviews must be provided or a stand-in appointed within this 30 minute time limit.
4. Once all feedback and edits have been synthesized by the `CMOC`, the `designated key approvers` should be tagged in slack for their final review and approval. This review and approval process should take no more than 30 minutes and approvers should mark their approval in the `approval matrix` at the top of the working incident response comms google doc.
5. When the communication(s) is/are final, our `Security External Communications` will work with the appropriate marketing and communications teams to deploy the communications to the appropriate external and internal audiences.   
6. Our `Security External Communications` will ensure all final materials and associated deployment details are noted in the related communications issue and close the issue.

### Designated key approvers

| Group | **Blog** | **Customer Email** | **FAQs for Support Teams** | **Media Response** | **Social/Forum Response** |
|------------------------------|------|----------------|------------------------|----------------|-----------------------|
| `VP of Security` | Approver | Approver | Approver | Approver if quote attributed to Security, FYI otherwise | FYI Only |
| `Senior Director of Legal, Privacy and Product` | Approver | Approver | Approver | Approver | Approver |
| `VP Customer Success` | Approver | Approver | FYI Only | Approver | FYI Only |
| [Support Manager on Call](https://about.gitlab.com/handbook/engineering/security/security-operations/sirt/security-incident-communication-plan.html#extended-team-roles-responsibilities-and-points-of-contact) | Approver | Approver | Approver  | FYI Only | FYI Only |
| `Director of Corp Comms` | Approver | Approver | FYI Only | Approver | Approver |
| `VP of Investor Relations` | Approver | Approver | FYI Only | Approver | FYI Only |

_Note: see [this confidential issue](https://gitlab.com/gitlab-com/gl-security/security-communications/communications/-/issues/440#approval-or-fyi-only) for contact details_

#### Designated back-up approvers (engaged if primary reviewer is unavailable or unresponsive) 
**- For the "required" approvers, we need at least 2 designated back-up approvers.**  

- Security: If `VP Security` is not available, `VP of Engineering` can approve, or, if not available, one of the `Security directors` may approve.
- Legal: `Senior Director of Legal, Privacy and Product` can approve. If not available, then move to `Legal Counsel`, and then move to `CLO`.  
- Customer Success: `VP Customer Success` can approve. If not available, `Snr Director, Technical Account Managers`, then `Director of Customer Success, Public Sector`.
- Support Team: `Support Manager on call` can approve. If not available, move to a member of [Support Senior Leadership](https://gitlab.com/groups/gitlab-com/support/managers/senior/-/group_members?with_inherited_permissions=exclude), then to [Support Managers](https://gitlab.com/groups/gitlab-com/support/managers/-/group_members?with_inherited_permissions=exclude) if needed.
- Corporate Comms: `Director of Corp Comms` can approve. If not available, move to `Manager, Public Relations` and then move to `Manager, Corp Comms` if needed. 
- Investor Relations: `VP of Investor Relations` can approve. If not available, move to `Director of Legal & Corporate Affairs`, and then move to `Chief Financial Officer` for back-up approval.

_Note: See [this confidential issue](https://gitlab.com/gitlab-com/gl-security/security-communications/communications/-/issues/440#designated-back-up-approvers) for contact details._

### Agreed upon response time for `designated key approvers`
**`Designated key approver` response time: 30 minutes**

Per our crisis comms firm and GitLab's Corporate Incident Response plans, each approver has 30 minutes to respond and review communications (providing feedback or edits) before we engage back-ups. Once feedback from the approvers has been received, reviewed and consolidated, a final review from approvers will be requested and each approver has 30 minutes response time to provide their approval. If your teams' review and approval is not achieved within 30 minutes and we have a consensus (75% of approvals received) of reviews and approvals, communications will move forward to finalization and deployment.

### Contacting `designated key approvers` on an urgent basis
* See [this confidential issue](https://gitlab.com/gitlab-com/gl-security/security-communications/communications/-/issues/440#contacting-approvers) for more information on how to contact `designated key approvers`.

#### Engaging designated backUp approvers
If 30 minutes has passed and the CMOC has not heard from a specific `designated key approver`, we move to engage all of that individual's `designated back-up approvers`in the incident-related slack channel and issue. If your teams' review and approval is not achieved within 30 minutes and we have a majority (75% of approvals received) of reviews and approvals, communications will move forward to finalization and deployment.


## Other helpful information

### Reference information
* [Security Incident Response Guide](https://about.gitlab.com/handbook/engineering/security/security-operations/sirt/sec-incident-response.html)
* [Security Communications Runbooks](https://gitlab.com/gitlab-com/gl-security/runbooks/-/tree/master/communications) (internal)
* [Incident Communications Plan](https://about.gitlab.com/handbook/engineering/security/security-operations/sirt/security-incident-communication-plan.html) 
* [Marketing Emergency Response process](https://about.gitlab.com/handbook/marketing/emergency-response/)
* [Time-sensitive blog post process](https://about.gitlab.com/handbook/marketing/blog/#time-sensitive-posts--instructions)  
* [Marketing rapid response process](https://about.gitlab.com/handbook/marketing/#marketing-rapid-response-process)

### Potential channels for use in a security incident

| Communications Channels | Purpose/Message | Additional Details |
| ------ | ------ | ------ |
| Incident Response Customer Email | Provides incident background, response, potential, impact, follow-up actions, and who to contact with questions. | Drafted by SIMOC/CMOC and reviewed by DRIs from Support, Legal, External Comms and Security. Sent from [incident-response@gitlab.com](incident-response@gitlab.com). Should be in **plain text** with **no link tracking**. If an accompanying blog post is published, blog should be linked. |
| Mitigation and response blog post | Details the background, GitLab response and any action required by our customers. | Developed when it is determined that a longer, more-in-depth response is needed, Content for the blog post is provided by the `SIMOC/CMOC`, and reviewed and approved by [designated key approvers](handbook/engineering/security/security-operations/sirt/security-incident-communication-plan.html#designated-key-approvers). The content team performs copyedits and merges. **Note: collaboration and work on the response blog post should happen in the related incident response channel on slack.** |
| GitLab Security Release Alert/Email | Indicates required action for customers and links to related mitigation and response blog. | Email sent to opt-in security notices distribution list. If a related blog post has been published, this email should include a link. Prepared and sent by `Security External Communications` or `Marketing Ops`, Sent to Security Notices distro through Marketo. Users can sign up for this distribution list through our [Communication Preference Center](/company/preference-center/). |
| Customer Frequently Asked Questions (FAQs) | List of early customer questions and responses, or probable questions and responses. | Created by `SIMOC/CMOC` and Support DRI. Provided to appropriate [Support group](/handbook/support/#channels). |
| Social media post | For distribution of related blog post, details our response to X issue. | `Security External Communications` engages `@community-team` in the incident response Slack channel. Provides Community Expert(s) with tweet text and blog link. GitLab social media team should also be alerted for ongoing awareness and monitoring, using `@social` in slack. |

### Sync meetings to develop and review customer communications
When appropriate, key stakeholders for contribution, review and approval should meet synchronously in a Zoom session to create and fine-tune customer communications (emails, FAQs, blog post, etc). Meeting synchronously in this case allows us to expedite the development of communications with key inputs from stakeholders in security, customer support and beyond and quickly move into the review stage.  These zoom sessions are recorded and will be linked into the related security incident external response issue.

