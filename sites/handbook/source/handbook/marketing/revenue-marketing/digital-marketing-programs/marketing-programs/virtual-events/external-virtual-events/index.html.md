---
layout: markdown_page
title: "External Virtual Events"
---

# Types of external virtual events

This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#overview](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#overview)

## 📌 Sponsored Webcast

This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#sponsored-webinars](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#sponsored-webinars)

### Process in GitLab to organize epic & issues

This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#sponsored-webinar-project-management](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#sponsored-webinar-project-management)

### Code for epic
This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#sponsored-webinar-epic-code](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#sponsored-webinar-epic-code)

## 📌 Sponsored Virtual Conference

This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#virtual-conferences](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#virtual-conferences)

### Process in GitLab to organize epic & issues


This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#virtual-conference-project-management](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#virtual-conference-project-management)

### Code for epic
This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#virtual-conference-epic-code](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#virtual-conference-epic-code)

## 📌 Executive Roundtable

This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#executive-roundtables](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#executive-roundtables)

### Process in GitLab to organize epic & issues

This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#executive-roundtable-project-management](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#executive-roundtable-project-management)

### Code for epic
This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#executive-roundtable-epic-code](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#executive-roundtable-epic-code)

## 📌 Vendor Arranged Meetings  


This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#vendor-arranged-meetings](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#vendor-arranged-meetings) 

### Process in GitLab to organize epic & issues

This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#vendor-arranged-meeting-project-management](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#vendor-arranged-meeting-project-management)

### Code for epic
This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#vendor-arranged-meeting-epic-code](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#vendor-arranged-meeting-epic-code)

## Adding external virtual events into the calendar

This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/#calendar-external-virtual-event](https://about.gitlab.com/handbook/marketing/virtual-events/#calendar-external-virtual-event)

##### Planned external virtual events

This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/#calendar-planned-external-virtual-event](https://about.gitlab.com/handbook/marketing/virtual-events/#calendar-planned-external-virtual-event)

##### Confirmed external virtual events 

This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/#calendar-confirmed-external-virtual-event](https://about.gitlab.com/handbook/marketing/virtual-events/#calendar-confirmed-external-virtual-event)

## Rescheduling external virtual events

This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/#calendar-rescheduled-external-virtual-event](https://about.gitlab.com/handbook/marketing/virtual-events/#calendar-rescheduled-external-virtual-event)

## Cancelling external virtual events
This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/#calendar-canceled-external-virtual-event](https://about.gitlab.com/handbook/marketing/virtual-events/#calendar-canceled-external-virtual-event)

## Post event
This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#sponsored-webinars](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#post-external-virtual-event)

###  Posting external virtual event recordings to youtube
This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#sponsored-webinars](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#post-youtube)

### Gating external webcasts
This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#sponsored-webinars](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#post-gating)

#### Non- Tech Partner (Alliances) webcasts
This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#sponsored-webinars](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#post-gating-with-alliance)

#### Tech Partner (Alliances) webcasts
This page has moved up to be directly under the Marketing handbook: [https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#sponsored-webinars](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/#post-gating-with-alliance)
