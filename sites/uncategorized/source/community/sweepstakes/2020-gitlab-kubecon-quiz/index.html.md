---
layout: markdown_page
title: 2020 GitLab KubeCon Quiz Sweepstakes 
description: "The KubeCon North America 2020 GitLab Quiz begins on November 17, 2020 at 10:00am (EST)  and ends on November 20, 2020 at 7:30pm (EST)"
canonical_path: "/community/sweepstakes/2020-gitlab-kubecon-quiz/"
---

## KubeCon North America 2020 GitLab Quiz - OFFICIAL RULES

NO PURCHASE NECESSARY TO ENTER OR WIN. MAKING A PURCHASE OR PAYMENT OF ANY KIND WILL NOT INCREASE YOUR CHANCES OF WINNING. VOID WHERE PROHIBITED OR RESTRICTED BY LAW. 

1.  PROMOTION DESCRIPTION: The KubeCon North America 2020 GitLab Quiz (\"Prize Draw\") begins on November 17, 2020 at 10:00am (EST) and ends on November 20, 2020 at 7:30pm (EST) (the \"Promotion Period\"). 

The sponsor of this Prize Draw is GitLab, Inc. ("Sponsor"). By participating in the Prize Draw, each Entrant unconditionally accepts and agrees to comply with and abide by these Official Rules and the decisions of Sponsor, which shall be final and binding in all respects. Sponsor is responsible for the collection, submission or processing of Entries and the overall administration of the Prize Draw. Entrants should look solely to Sponsor with any questions, comments or problems related to the Prize Draw. Sponsor may be reached by email at tshurtleff@gitlab.com during the Promotion Period. 

2.  ELIGIBILITY: Open to registrants of KubeCon North America 2020 who are eighteen (18) or older (the \"Entrant\"). 

Sponsor, and their respective parents, subsidiaries, affiliates, distributors, retailers, sales representatives, advertising and promotion agencies and each of their respective officers, directors and employees (the \"Promotion Entities\"), are ineligible to enter the Prize Draw or win a prize. Household Members and Immediate Family Members of such individuals are also not eligible to enter or win. "Household Members" shall mean those people who share the same residence at least three months a year. "Immediate Family Members" shall mean parents, step-parents, legal guardians, children, step-children, siblings, step-siblings, or spouses. 

Any person located in a “Restricted Country” is not eligible. Restricted Countries are those countries with asset freeze sanctions from the United States during the Promotion Period. 

Residents of Quebec are not eligible.

This Prize Draw is subject to all applicable United States federal, state and local laws and regulations and is void where prohibited or restricted by law. 

3. PRIZES: 


One (1) winner will receive one (1) Bose QuietComfort Earbuds (approximate retail value or "ARV": $279.95)
One (1) winner will receive one (1) Oculus Quest 2 — Advanced All-In-One Virtual Reality Headset (approximate retail value or "ARV": $299)
Two (2) winners will receive one (1) Uber Eats Gift Card (approximate retail value or “ARV”: $100) 


Only one prize per person and per household will be awarded. Gift cards and gift certificates are subject to the terms and conditions of the issuer. Prizes cannot be transferred, redeemed for cash or substituted by winner. Sponsor reserves the right in its sole and absolute discretion to award a substitute prize of equal or greater value if a prize described in these Official Rules is unavailable or cannot be awarded, in whole or in part, for any reason. The ARV of the prize represents Sponsor's good faith determination. That determination is final and binding and cannot be appealed. If the actual value of the prize turns out to be less than the stated ARV, the difference will not be awarded in cash. Sponsor makes no representation or warranty concerning the appearance, safety or performance of any prize awarded. Restrictions, conditions, and limitations may apply. Sponsor will not replace any lost or stolen prize items.
    
This Prize Draw is not open to persons in Restricted Countries and Prize will not be awarded and/or delivered to addresses within said locations. 

All federal, state and/or local taxes, fees, and surcharges are the sole responsibility of the prize winner. Failure to comply with the Official Rules will result in forfeiture of the prize.
    
4. HOW TO ENTER: Entrants enter by correctly answering the quiz questions in GitLab's KubeCon North America 2020 digital booth, accessible through the KubeCon North America 2020 virtual expo hall.  For each question answered correctly, the Entrant will receive one (1) entry into the Prize Draw.  Entrants may only complete the quiz questions one (1) time. 

Automated or robotic Entries submitted by individuals or organizations will be disqualified. Internet entry must be made by the Entrant. Any attempt by Entrant to obtain more than the stated number of Entries by using multiple/different email addresses, identities, registrations, logins or any other methods, including, but not limited to, commercial contest/Prize Draw subscription notification and/or entering services, will void Entrant's Entries and that Entrant may be disqualified. Final eligibility for the award of any prize is subject to eligibility verification as set forth below. All Entries must be posted by the end of the Promotion Period in order to participate. Sponsor's database clock will be the official timekeeper for this Prize Draw.
    
5. WINNER SELECTION: The Winner(s) of the Prize Draw will be selected in a random drawing from among all eligible Entries received throughout the Promotion Period. The random drawing will be conducted with one (1) week after the Promotion Period by Sponsor or its designated representatives, whose decisions are final. Odds of winning will vary depending on the number of eligible Entries received.

6. WINNER NOTIFICATION: Winner will be notified by email at the email address provided in the Entry Information within one (1) week after the random drawing. Potential Winner must accept a prize by email as directed by Sponsor within ninety (90) days of notification. Sponsor is not responsible for any delay or failure to receive notification for any reason, including inactive email account(s), technical difficulties associated therewith, or Winner’s failure to adequately monitor any email account.

Any winner notification not responded to or returned as undeliverable may result in prize forfeiture. The potential prize winner may be required to sign and return an affidavit of eligibility and release of liability, and a Publicity Release (collectively \"the Prize Claim Documents\"). No substitution or transfer of a prize is permitted except by Sponsor.

7. PRIVACY: Any personal information supplied by you will be subject to the privacy policy of the Sponsor posted at https://about.gitlab.com/privacy/. By entering the Prize Draw, you grant Sponsor permission to share your email address and any other personally identifiable information with the other Prize Draw Entities for the purpose of administration and prize fulfillment, including use in a publicly available Winners list.

If you wish to assert your rights to information, correction, deletion, restriction of processing, object to data processing or revoke your consent to data processing, please use our [Personal Data Request Form](https://support.gitlab.io/account-deletion/).

8. LIMITATION OF LIABILITY: Sponsor assumes no responsibility or liability for (a) any incorrect or inaccurate entry information, or for any faulty or failed electronic data transmissions; (b) any unauthorized access to, or theft, destruction or alteration of entries at any point in the operation of this Prize Draw; (c) any technical malfunction, failure, error, omission, interruption, deletion, defect, delay in operation or communications line failure, regardless of cause, with regard to any equipment, systems, networks, lines, satellites, servers, camera, computers or providers utilized in any aspect of the operation of the Prize Draw; (d) inaccessibility or unavailability of any network or wireless service, the Internet or website or any combination thereof; (e) suspended or discontinued Internet, wireless or landline phone service; or (f) any injury or damage to participant's or to any other person’s computer or mobile device which may be related to or resulting from any attempt to participate in the Prize Draw or download of any materials in the Prize Draw.

If, for any reason, the Prize Draw is not capable of running as planned for reasons which may include without limitation, infection by computer virus, tampering, unauthorized intervention, fraud, technical failures, or any other causes which may corrupt or affect the administration, security, fairness, integrity or proper conduct of this Prize Draw, the Sponsor reserves the right at its sole discretion to cancel, terminate, modify or suspend the Prize Draw in whole or in part. In such event, Sponsor shall immediately suspend all drawings and prize awards, and Sponsor reserves the right to award any remaining prizes (up to the total ARV as set forth in these Official Rules) in a manner deemed fair and equitable by Sponsor. Sponsor and Released Parties shall not have any further liability to any participant in connection with the Prize Draw.

9. WINNER LIST/OFFICIAL RULES: To view a copy of the Winner List or a copy of these Official Rules, visit https://about.gitlab.com/events/kubecon-na. The winner list will be posted after winner confirmation is complete.

10. GOVERNING LAW: This Prize Draw is governed by United States law. 

11. SPONSOR: GitLab, Inc., 268 Bush Street, #350, San Francisco, CA 94104, tshurtleff@gitlab.com




