---
layout: handbook-page-toc
title: "E-group offsite"
description: "The E-group offsite happens every quarter for four days after the Board of Directors meeting"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The E-group offsite happens every quarter for four days after the [Board of Directors meeting](/handbook/board-meetings/#board-meeting-schedule){:data-ga-name="Board meeting schedule"}{:data-ga-location="body"}

## Goal

The goal is to have 25 or 50 minute discussions around topics that benefit from in-person dialogue, require more context and where the E-Group is likely to disagree.
The agenda should include discussions that are:

1. Top of mind
1. Actionable
1. Impactful to the trajectory of the company
1. Cross-functional

## Attendees

1. [Executives](/company/team/structure/#executives){:data-ga-name="executives"}{:data-ga-location="body"}
1. [Chief of Staff to the CEO](/job-families/chief-executive-officer/chief-of-staff/){:data-ga-name="chief of staff"}{:data-ga-location="body"}; when not possible, the [Director, Strategy and Operations](/job-families/chief-executive-officer/strategy-and-operations/){:data-ga-name="director strategy and operations"}{:data-ga-location="body"}
1. [CEO Shadows](/handbook/ceo/shadow/){:data-ga-name="CEO shadow"}{:data-ga-location="body"}
1. [Executive Business Admin](/handbook/eba/#executive-business-administrator-team){:data-ga-name="executive business admin"}{:data-ga-location="body"} to the CEO (optional)
1. Invited participants: folks invited to participate in one or more specific session

#### Roles

**Executives** are committed to working through difficult discussions and problems during the event.
They commit to being active participants by proposing topics to the agenda before hand and being executive sponsors for those discussion topics.
They establish criteria for success for the discussion topics by, for example, identifying decisions to be made or establishing clarity on what the next steps need to be.
If decisions need to be made to stop, continue, or change discussion when there is disagreement on path forward, the decision is responsibility of the topic executive sponsor.

The **CEO Shadows** are responsible for taking thorough notes throughout the event, so that the E-Group can be focused on the discussion.
CEO Shadows will also tasked with making Merge Requests on behalf of an Executive.
Please follow the below outlined process for announcing and merging the changes.

The **Chief of Staff to the CEO** or other team member is responsible for facilitating the event.
They will work with the Staff EBA closely to ensure the event runs smoothly.
The CoS to the CEO is the on-the-ground person ensuring that the event is kept on-schedule, discussions are kept on-subject, helping steer the conversation when necessary, guiding conversations towards action items, and ensuring that [implementation is about 50% of time](/company/offsite/#timeline){:data-ga-name="timeline"}{:data-ga-location="body"}.

The **Staff Executive Business Administrator to the CEO** is responsible for organizing and coordinating the Offsite, including travel, lodging, agendas, and meals.

## Logistics

Since most of the E-group is in the San Francisco Bay Area, we'll go to a location that is drivable or a short direct flight, for example: East Bay, Denver, Sonoma.
We tend to pick a location at or close to a member of the E-group.
If the CEO is traveling, the CoS to the CEO (preferred) or the Staff EBA should bring [the Owl](https://www.owllabs.com).

[Hybrid calls are hard](/handbook/communication/#hybrid-calls-are-horrible){:data-ga-name="hybrid calls are hard"}{:data-ga-location="body"}, but occasionally the Offsite will need to take a hybrid meeting form.
When this is the case, the Staff EBA to the CEO will ensure that the calendar invite for the Zoom offsite includes a Zoom link.
The Zoom link should have a waiting room attached to it, since the zoom URL is on calendars and is discoverable internally.
This also allows the E-group to pull in folks as-needed into the room without switching Zoom rooms, as people won't just jump in and out without being noticed or before the E-group is ready to move onto that subject.

#### Offsites in the era of COVID-19

COVID-19 has lead GitLab to [suspend all non-essential travel](/handbook/travel/#travel-guidance-covid-19){:data-ga-name="suspend all non-essential travel"}{:data-ga-location="body"}.
That includes the E-group offsite.

In the circumstance of a fully remote e-group offsite, instead of 2.5 days of in-person meetings, the e-group offsite occurs over 4 all-remote half-day sessions.
This is because some people may find Zoom makes the meeting more intense.
The CEO Skips follow up call will happen on the business day immediately following the final inclusion.

## Schedule

The off-site is a quarterly meeting scheduled over 4 days (including travel time).
The offsite meeting should take place during the second or third month of the quarter to avoid conflicts with Sales QBRs and occurs after quarterly Board of Director meetings and post quarterly earning calls.
Scheduling for the in-person event generally follows:
1. Travel Day: travel day with optional dinner around 7pm local time (usually Monday).
1. First Day: Full day meeting starting with breakfast at 8am (usually Tuesday)
1. Second Day: Full day meeting starting with breakfast at 8am. When the event does not coincide with Contribute, Sr. EBA to the CEO will coordinate an off-site activity for the afternoon (usually Wednesday).
1. Third Day: Half day meeting starting with breakfast at 8am. Usual end time is 12:30 pm with departure flights scheduled in the late afternoon (usually Thursday).
1. First day after third day: The E-group hosts a [CEO Skips zoom call](#ceo-skips-zoom-call) (usually Friday or the following Monday).

E-Group is welcome to fly in early or stay later pending their travel preferences.

Scheduling for a remote offsite generally follows:
1. Monday through Thursday from 8am-12:20pm Pacific time each day.
1. The following Monday after the offsite concludes, the E-Group hosts a [ceo skips zoom call](#ceo-skips-zoom-call)

It generally occurs in:
* March or early April
* June or early July
* October
* January

In 2021, the following offsite dates have been confirmed (starting dates):
* 2021-06-28
* 2021-11-01
* 2022-01-10
* 2022-04-04
* 2022-07-11
* 2022-10-03

## Timeline

1. 2 weeks before Content Selection or other required pre-work is finalized
1. 2 weeks before- if a subject requires data to support the discussion, make a request of [the data team](/handbook/business-ops/data-team/){:data-ga-name="the data team"}{:data-ga-location="body"} for assistance no less than 2 weeks before the offsite; [alert the data team of on-call request](/company/offsite/#data-team-member-on-call){:data-ga-name="alert the data team of on call request"}{:data-ga-location="body"}
1. Friday before- agenda is finalized (though always subject to change); Prep Work asks finalized
1. Immediately following the Offsite, the CoST to the CEO should create the agenda for the next Offsite so that topics can be added as they come up.

## Prep Work

Because the Offsite is a very expensive meeting, we want to be sure we are as efficient as possible.
One of the ways we do this is by asking participants to do prep work ahead of time.

Examples of prep work include:
* [Reading the content that was selected](/company/offsite/#content-choice){:data-ga-name="content choice"}{:data-ga-location="body"}
* Executives working with their People Business Partners to prep the [Performance/Potential Matrix of their direct reports](/handbook/people-group/performance-assessments-and-succession-planning/#the-performancepotential-matrix){:data-ga-name="performance portential matrix"}{:data-ga-location="body"}
* Executives working with their People Business Partners to prep any [Director + promotion nominations](/handbook/people-group/promotions-transfers/#bamboohr-or-greenhouse-process){:data-ga-name="director + promotion nominations"}{:data-ga-location="body"}
* Reviewing any Section Direction Materials for reviews, such as the [Growth Section's Direct](/direction/growth/)
* [Watch this short clip on the danger of sticky bears](/company/offsite/beware-sticky-bears/){:data-ga-name="growth"}{:data-ga-location="body"}

Anyone who is presenting for a topic should share materials with the prep work.

Prep work should be shared *at least* 1 business day before. EBAs should be cc'd, as some execs may want time carved out on their calendars for the review.

## Guidance for invited participants

There are a number of reasons why participants are invited to join specific Offsite meetings. These can range from giving team members a chance for greater context to expecting an invited participant to lead a discussion. Before you attend the Offsite, please:

1. Understand expectations for your participation 
1. Ensure that you have context about what is being discussed. If you are invited to a meeting and don't have context, proactively start an async conversation or schedule a 15 minute meeting with the E-Group sponsor and other participants. You should not need to attend a meeting without knowing what will be discussed or do any pre-work without clear direction
1. **Don't** spend hours preparing unless there is a direct ask to do so

## Data team member on call

A data team member is available on-call during the offsite.
The CoS to the CEO gives the data team a heads up at least two weeks before, so that they data team can plan appropriately.

## Finance business partner available in case of questions

A finance business partner is available during offsite agenda sessions related to reviews of the financials or budgets.
The CoS to the CEO gives the finance team exec admin and fp&a team a heads up at least two weeks before, so that the finance team can plan and block calendars appropriately.

## Agenda and Documenting

We take notes in a Google Doc that contains the SSoT of the agenda.
At least 1 month prior, the doc is created and proposed topics for discussion should be added to the bottom of the doc. Each proposed topic should include a clear definition of what we are trying to achieve in the discussion (a decision made, a chance for E-Group to give feedback, etc.). E-Group sponsors are responsible for providing context to team members who they plan to include in discussion preparation or participation. This could take the form of a 15 minute meeting to align on goals for the discussion and what material should be prepared in advance. 
(The CoS to the CEO will pull from here when prioritizing and planning.)
Please add links to relevant materials, issues, or proposals up front.
When there is an issue or doc linked, we take notes there, instead of in the overall doc.
There is a [doc template](https://docs.google.com/document/d/1LCy1qWG88ChBXWL8YibNJbkxuxzVdh0Wx9KM1lw_Vcg/edit) that can be used as a starting point.

If we can conclude a topic early we move on to one from a reserve list.
The CoS to the CEO is responsible for maintaining the schedule, optimizing discussion schedules for energy levels, and having topics prepared.

The agenda will guide conversations. If an E-Group member wants to speak, the team member should put thoughts in the agenda. In instances where the comment cannot be typed out, for reasons of confidentiality or given time constraints in typing out the comment, the member can type out: [INSERT NAME:!]. This serves a time placeholder for the team member, so the team member can contribute at the appropriate point in the agenda.

We will document agreed changes directly to the handbook and any other relevant SSoT during the meeting.
Every item will get a MR maker and MR reviewer assigned.
Most of the time the MR maker will be the CoS to the CEO, one of the CEO shadows, or the Staff EBA to the CEO.
When the MR is ready, the reviewer is at-mentioned in the public E-group channel in Slack.
The reviewer communicates with the maker via that Slack thread.
The goal is to merge it the same day, preferably within 15 minutes.

## Communication guidelines

We have communication guidelines to enable more efficient and productive conversations:
1. Encourage participation from others. We should actively encourage folks who aren't speaking much to speak--especially if we are discussing a topic that should have their direct input. This includes directly asking other folks for their opinions and allowing space for folks to join the conversation.
1. Proactively stop conversations only involving two people if they go on for more than a few minutes and do not require engagement from the broader group. These conversations can be taken offline. 

## Topic owner responsibilities

If you are the person who owns a topic, you are responsible for:
1. Ensuring that any pre-work is shared at least 5 days before the offsite.
1. The conversation is being managed toward your desired outcome.
1. Encouraging folks to stick to the agenda rather than speaking out of order. 
1. The conversation includes all folks who want to contribute. This may mean actively encouraging folks to participate if they have not spoken and may have an item to add.

### Recurring discussion topics

There are some topics discussed at every E-group offsite.
These include:
1. Reviewing action items from the last offsite
1. [Director+ Promotions](/handbook/people-group/promotions-transfers/#bamboohr-or-greenhouse-process){:data-ga-name="director + promotions"}{:data-ga-location="body"} (as-needed based on number of people, estimate ~10 mins per person)
1. [Content Discussion](/company/offsite/#content-choice){:data-ga-name="content discussion"}{:data-ga-location="body"} (30 minutes - 1 hour)
1. Spend prioritization discussion (50 minutes)
1. [2x per year] [Talent Assessment and Succession Planning](/handbook/people-group/performance-assessments-and-succession-planning/){:data-ga-name="talent assesment"}{:data-ga-location="body"} (1.5 hrs, ~10 mins per function). CEO Shadows and EBAs do not attend this session
1. [Annual] Strategic planning discussions (~1 day) 

### Team norms

We agreed to E-Group team norms at our 2022-01 offsite. In our discussions, we'll consider the following:

1. Is this topic worth debating?
1. What should we do to evaluate the priority of this idea?
1. The why behind the what. How will we communicate the rationale behind change/priority/direction via multi-modal communication and repetition?

### Break guidelines 

Break periods are scheduled into the agenda by the CoS to the CEO and should follow these guidelines:
1. Breaks should be scheduled for 1/6 of the time (same as speedy meeting)
1. Breaks should be a minimum of 5 minutes
1. Going over time on any topic should result in moving the break. It does not reduce it.  


## Follow up

Every discussion should start by clarifying "What decision needs to be made from this?"
All follow up actions need to be captured as to-dos, noted with `TODO Person` in the doc.
If there is not an exec's name tied to the to-do, it belongs to the CEO Shadows/and or the CoS to the CEO.
Before emphasizing follow ups, many conclusions never landed and/or resulted into action.
Follow ups can take the form of an:

1. Merge request to the handbook
1. Issue created
1. Meeting scheduled
1. Notes shared with the rest of the company in Slack
1. Etc.

## CEO Skips Zoom Call

CEO Skips are a key leadership team that plays a key role in identifying and realizing business opportunities.  CEO Skip input is valuable in the lead up to meetings, and CEO Skip collaboration is key for cascading efforts that follow.

In advance of the offsite, E-group members are expected to connect with their team leadership to identify possible agenda topics, get input into specific topics, and share an overview of what is being discussed. The goal is not to waste an opportunity for broader input and insight from the broader leadership team. The CEO-Skip team should arrive at our CEO Skip Zoom call already having an understanding of some of key items that were discussed.

In the week following the Offsite, there is a 50-minute Zoom call before 12pm Pacific time for the [CEO Skips Group](/company/team/structure/#ceo-skips).
This time will not work for everyone. The meeting will be privately streamed to Unfiltered on YouTube.

The goal of this call is to communicate:
1. Strategic or visionary updates that are crucial to priorities
1. Key decisions made
1. Key messaging that leaders are enlisted to help distribute
1. Action items which may need cross-functional collaboration

The Agenda will be organized by meeting topics. Designated CEO Skip team members who led or participated in specific sessions will provide written summaries and flag highlights. CEO Skip team members can put their comments and questions below each summary. We will run through the agenda during the meeting.

CEO Skips play a key role in cascading what is happening in the business and communicating key implications for teams and team members. After each CEO Skip Meeting, CEO Skip members should:
1. Identify key changes or information of greatest relevance to their teams
1. Reach out to their manager if they have unresolved questions
1. Recognize any sensitivities and think about how best to frame messaging to the team
1. Communicate key insights and asks for team members
1. Help to operationalize any asks or decisions that have been made

Notes shared with CEO Skips will be shared with all team members within 4 business days of an CEO Skips Meeting. Notes from the AMA and the video recording will not be shared more broadly.

This Zoom call is a separate invite.
The Staff EBA to the CEO is responsible for setting up the invite, the doc for the call, and inviting CEO Skips.
The CoS to the CEO will moderate the call.

The CoS to the CEO will have materials prepared for the call, no later than 12 business hours before or before end of day if the offsite concludes on a Friday. Material will include an event summary and a highlight video message.
The materials used for the call can be repurposed by attendees to share key information with their teams.
Think of the materials as a "Meeting in a Box."

### CEO-Skips Async Debrief

The debrief for the 2021-06-29 to 2021-06-30 E-Group Offsite was conducted async. This was because this was a shorter offsite, so there was less information to share. 

## Content Choice

The offsite includes a 30 minute to 1 hour discussion on material chosen by the CEO. This could be a book, article, recording, or other artifact. The selection will be finalized no less than two weeks prior to the offsite. At the time that the selection is shared, the CEO will also share three conversation topics. E-Group team members are expected to come to the discussion prepared to discuss these prompts.

We will share discussion highlights and takeaways in E-Group Offsite meeting notes that are shared with all team members. This also allows team members to engage in the conversation.

Team Members may [expense](/handbook/spending-company-money/#expense-policy){:data-ga-name="expense"}{:data-ga-location="body"} E-Group offsite material in the quarter that it is discussed.

Material that the E-Group finds noteworthy should be added to the [Leadership Books](/handbook/leadership/#books){:data-ga-name="leadership books"}{:data-ga-location="body"}.

### Previous Reads

* [So You want to Talk about Race](https://www.goodreads.com/book/show/35099718-so-you-want-to-talk-about-race) by Ijeoma Oluo
* [Dare to Lead](https://www.goodreads.com/book/show/40109367-dare-to-lead) by Brené Brown
* [Better Allies](https://www.goodreads.com/book/show/43545460-better-allies) by Karen Catlin
* [Great at Work](https://www.goodreads.com/book/show/35297611-great-at-work) by Morten Hansen
* [Leadership and Self-Deception](https://www.goodreads.com/book/show/180463.Leadership_and_Self_Deception) by the Arbinger Institute
* [Crucial Conversations](https://www.goodreads.com/book/show/15014.Crucial_Conversations) by Kerry Patterson, Joseph Granny, Ron McMillan, & Al Switzler
* [The Servant: A simple story about the true essence of leadership](https://www.goodreads.com/book/show/181736.The_Servant) by James Hunter
* [Drive: The Surprising Truth About What Motivates Us](https://www.goodreads.com/book/show/6452796-drive) by Daniel Pink
